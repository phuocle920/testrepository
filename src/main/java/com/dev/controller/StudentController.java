package com.dev.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping("/hello")
public class StudentController {

	@RequestMapping(method = RequestMethod.GET)
	public String helloSpring(ModelMap model){
		model.addAttribute("message", "Hello Spring MVC framework !!");
		return "index";
	}
}
